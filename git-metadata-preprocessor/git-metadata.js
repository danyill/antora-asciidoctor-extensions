'use strict'

const fs = require('fs')
const git = require('isomorphic-git')
const moment = require('moment-timezone')
const path = require('path')

git.plugins.set('fs', fs)

/*
* Given a repo with a lightweight or annotated return the associated commit
*
* @param {String} dir - The path to the git repository
* @param {String} tag - The tag for which the commit is desired
*
* @returns {String} oid - The commit hash of the tag
*
*/
async function getTagCommit (dir, tag) {
  let oid = await git.resolveRef({ dir: dir, ref: 'refs/tags/' + tag })
  let obj = await git.readObject({ dir: dir, oid: oid })

  if (obj.type !== 'commit') {
    // reading the next level reference to reach commit
    oid = obj.object.object
  }
  return oid
}

/**
 * This preprocessor Asciidoctor extension provides some predefined document attributes
 * associated with git metadata. It is intended for use with asciidoctor.js or Antora
 *
 * It provides a map with the following information
 *
 * * latest commit sha: sha
 * * short (7-char) commit sha: sha-short
 * * latest commit author: author-name
 * * latest commit author email: author-email
 * * latest commit date: date
 * * latest commit time: time
 * * latest commit timezone: timezone
 * * latest commit message: commit-message
 * * the current branch: branch
 * * the current tags(s): tag} which may be either lightweight or annotated
 * * the path to this repository: repo-path
 * * the relative path from this file to the repository root: relative-path
 * * the `origin` remote if exists: remotes-origin
 *
 * @param {String} dirOrSubdir - The path to the git repository
 * @param {String} currentFilePath - The location of the current file being processed
 *
 * The console is used to return this data as a map using JSON
 *
 */
async function getMetadata (dirOrSubdir, currentFilePath) {
  let dir
  try {
    dir = await git.findRoot({ filepath: dirOrSubdir })
  } catch (e) {
    if (e instanceof git.GitRootNotFoundError) {
      return
    }
  }
  const branch = await git.currentBranch({ dir: dir, fullname: false })
  const sha = await git.resolveRef({ dir: dir, ref: 'HEAD' })
  const commitInfo = await git.log({ dir: dir, depth: 1, ref: sha })

  const timeStamp = commitInfo[0].author.timestamp * 1000
  const date = new Date(timeStamp)
  const localTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone // eg. 'America/Chicago'
  const timeZoneAbbrev = (moment(date).tz(localTimeZone).format('z'))

  // there must be a better way...
  const dateStr = date.getFullYear().toString() + '-' +
                  (date.getMonth() + 1).toString().padStart(2, '0') + '-' +
                  date.getDate().toString().padStart(2, '0')
  const timeStr = date.getHours().toString().padStart(2, '0') + ':' +
                  date.getMinutes().toString().padStart(2, '0') + ':' +
                  date.getSeconds().toString().padStart(2, '0')

  const tags = await git.listTags({ dir: dir })

  let headTags = []
  // iterate through tags to check if their commit matches HEAD
  if (tags && tags.length) {
    for (const tag of tags) {
      let oid = await getTagCommit(dir, tag)
      if (oid === sha) {
        headTags.push(tag)
      }
    }
  }

  const remotes = await git.listRemotes({ dir: dir })

  // iterate remotes map and find origin, why aren't these key-ed against the short name?
  let remoteOrigin
  for (const remote of remotes) {
    if (remote['remote'] === 'origin') {
      remoteOrigin = remote['url']
    }
  }

  return { 'sha': sha,
    'sha-short': sha.slice(0, 7),
    'branch': branch,
    'author-name': commitInfo[0].author.name,
    'author-email': commitInfo[0].author.email,
    'commit-message': commitInfo[0].message,
    'date': dateStr,
    'time': timeStr,
    'timezone': timeZoneAbbrev,
    'tag': headTags.join(', '),
    'relative-path': path.relative(path.dirname(currentFilePath), dir),
    'repo-path': dir,
    'remotes-origin': remoteOrigin,
  }
}

(async () => {
  const args = process.argv.slice(2)
  const options = {}
  for (let j = 0; j < args.length; j++) {
    const arg = args[j]
    if (arg.startsWith('--dir=')) {
      options.dir = arg.slice('--dir='.length)
    } else if (arg.startsWith('--file-path=')) {
      options.filePath = arg.slice('--file-path='.length)
    }
  }
  const gitData = await getMetadata(options.dir, options.filePath)
  console.log(JSON.stringify(gitData))
})()

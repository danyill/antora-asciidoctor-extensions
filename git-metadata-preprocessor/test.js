'use strict'

const asciidoctor = require('@asciidoctor/core')()

const registry = asciidoctor.Extensions.create()
require('./extension.js').register(registry)

asciidoctor.convertFile('sample.adoc', { extension_registry: registry, 'attributes': { 'safe': 'safe' } })

'use strict'

const path = require('path')

/**
 * This preprocessor Asciidoctor extension provides some predefined document attributes
 * associated with git metadata. It is intended for use with asciidoctor.js or Antora
 *
 * It provides:
 *
 * * latest commit sha: {git-metadata-sha}
 * * short (7-char) commit sha: {git-metadata-sha}
 * * latest commit author: {git-metadata-author-name}
 * * latest commit author email: {git-metadata-author-email}
 * * latest commit date: {git-metadata-date}
 * * latest commit time: {git-metadata-time}
 * * latest commit timezone: {git-metadata-timezone}
 * * latest commit message: {git-metadata-commit-message}
 * * the current branch: {git-metadata-branch}
 * * the current tags(s): {git-metadata-tag} which may be either lightweight or annotated
 * * the path to this repository: {git-metadata-repo-path}
 * * the relative path from this file to the repository root: {git-metadata-relative-path}
 * * the `origin` remote if exists: {git-metadata-remotes-origin}
 *
 * @param {Object} context - The context within which the extension was run
 *
 * @returns {Object} reader - The asciidoctor reader
 *
 * @author Dan Mulholland <dan.mulholland@gmail.com>, <ggrossetie@gmail.com>
 */
function gitMetadataPreprocessor (context) {
  return function () {
    const self = this

    self.process(function (doc, reader) {

      let dir, filePath

      if (typeof context.contentCatalog !== 'undefined' &&
      typeof context.contentCatalog.addFile !== 'undefined' &&
      typeof context.contentCatalog.addFile === 'function' &&
      typeof context.file !== 'undefined') {
        // Antora context
        dir = context.file.src.origin.cacheDir
        filePath = path.join(dir, context.file.src.origin.startPath, context.file.src.path)
      } else if (typeof dir === 'undefined' && typeof filePath === 'undefined') {
        // filesystem
        dir = reader.dir
        filePath = path.join(reader.dir, reader.file)
      } else {
        return
      }

      const output = require('child_process').execSync(`"${process.argv[0]}" "${__dirname}/git-metadata.js" \
--dir="${dir}" \
--file-path="${filePath}"`, { stdio: 'pipe' })
      const gitData = JSON.parse(output.toString('utf8'))
      Object.keys(gitData).forEach(function (attrSuffix) {
        // set the document attributes
        let attributeName = 'git-metadata-' + attrSuffix
        if (gitData[attrSuffix].length) {
          doc.setAttribute(attributeName, gitData[attrSuffix])
        } else {
          doc.setAttribute(attributeName, '{' + attributeName + '}')
        }
      })
      return reader
    })
  }
}

module.exports.register = function (registry, context = {}) {
  registry.preprocessor(gitMetadataPreprocessor(context))
  return registry
}
